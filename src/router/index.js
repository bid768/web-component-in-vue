import Vue from 'vue'
import Router from 'vue-router'
import Layout from '../components/layout'
import Modal from '../components/modal'
import VueHello from '../components/vueHello'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: { name: 'vueHello' }
    },
    {
      path: '/layout',
      name: 'layout',
      component: Layout
    },
    {
      path: '/modal',
      name: 'modal',
      component: Modal
    },
    {
      path: '/vueHello',
      name: 'vueHello',
      component: VueHello
    }
  ]
})
